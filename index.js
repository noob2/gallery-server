const express = require('express')
const fs = require('fs')
const path = require('path')
const multer = require('multer')
const PORT = 4000

const app = express()

const imagesDirectory = path.join(__dirname, 'sourceimages_medium')
const likedImagesFilePath = path.join(__dirname, 'liked_images.json')

// Multer configuration
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, imagesDirectory)
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + path.extname(file.originalname))
  }
})

const upload = multer({ storage })

app.use(express.json()) // To parse JSON request bodies

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
  next()
})

app.get('/allImages', (req, res) => {
  fs.readdir(imagesDirectory, (err, files) => {
    if (err) {
      console.error('Error reading directory:', err)
      return res.status(500).json({ error: 'Server error' })
    }

    const imageFiles = files.filter(file => ['.jpg', '.jpeg', '.png', '.gif'].includes(path.extname(file).toLowerCase()))

    return res.json({ images: imageFiles })
  })
})

app.get('/image/:imageName', (req, res) => {
  const imageName = req.params.imageName
  const imagePath = path.join(imagesDirectory, imageName)

  fs.readFile(imagePath, (err, data) => {
    if (err) {
      console.error('Error reading image file:', err)
      return res.status(404).json({ error: 'Image not found' })
    }

    res.setHeader('Content-Type', 'image/jpeg')
    res.send(data)
  })
})

// Route to handle liking an image
app.post('/likeImage/:imageName', (req, res) => {
  const imageName = req.params.imageName

  // Read the list of liked images from the file (if exists)
  fs.readFile(likedImagesFilePath, 'utf8', (err, data) => {
    let likedImages = []

    if (!err) {
      try {
        likedImages = JSON.parse(data)
      } catch (parseErr) {
        console.error('Error parsing liked images data:', parseErr)
      }
    }

    // Check if the image is already liked
    const imageIndex = likedImages.indexOf(imageName)
    if (imageIndex !== -1) {
      // If the image is already liked, remove it from the liked images list
      likedImages.splice(imageIndex, 1)
    } else {
      // If the image is not liked, add it to the liked images list
      likedImages.push(imageName)
    }

    // Write the updated liked images list to the file
    fs.writeFile(likedImagesFilePath, JSON.stringify(likedImages), writeErr => {
      if (writeErr) {
        console.error('Error writing liked images data:', writeErr)
        return res.status(500).json({ error: 'Failed to update liked images' })
      }

      return res.status(200).json({ success: true, liked: imageIndex === -1 })
    })
  })
})

// Route to get the list of liked images
app.get('/likedImages', (req, res) => {
  fs.readFile(likedImagesFilePath, 'utf8', (err, data) => {
    if (err) {
      console.error('Error reading liked images data:', err)
      return res.status(500).json({ error: 'Failed to fetch liked images' })
    }

    let likedImages = []
    try {
      likedImages = JSON.parse(data)
    } catch (parseErr) {
      console.error('Error parsing liked images data:', parseErr)
      return res.status(500).json({ error: 'Failed to fetch liked images' })
    }
    return res.json({ likedImages })
  })
})

app.delete('/image/:imageName', (req, res) => {
  const imageName = req.params.imageName
  const imagePath = path.join(imagesDirectory, imageName)

  // Check if the image file exists
  fs.access(imagePath, fs.constants.F_OK, err => {
    if (err) {
      console.error('Error reading image file:', err)
      return res.status(404).json({ error: 'Image not found' })
    }

    // Delete the image file
    fs.unlink(imagePath, unlinkErr => {
      if (unlinkErr) {
        console.error('Error deleting image:', unlinkErr)
        return res.status(500).json({ error: 'Failed to delete image' })
      }

      return res.status(200).json({ success: true })
    })
  })
})

app.post('/uploadImage', upload.single('image'), (req, res) => {
  const imageUrl = req.file.filename
  return res.status(200).json({ imageUrl })
})

app.listen(PORT, () => console.log(`Server listening on port ${PORT}`))
